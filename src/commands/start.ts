import { ConsolePrinter } from './../core/console-printer.service';
import { ICommand } from './../types/command';
import { ExecutionResult } from '../types/execution-result';
import MainGame from '../tamagochi-game/main';

export class StartCommand implements ICommand {

  constructor(
    private readonly printer: ConsolePrinter = new ConsolePrinter(),
  ) { }

  public async execute(): Promise<ExecutionResult> {
    // this.printer.print(
    //   `
    //   Starting Tamagochi Game...
    //   `
    // );

    const game = new MainGame()
    game.startGame()

    return { errors: 0, message: undefined };
  }

}