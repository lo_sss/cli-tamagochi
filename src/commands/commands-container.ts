import { StartCommand } from './start';
import { HelpCommand } from './help';
export class CommandContainer {
  // _command1: string
  readonly help: HelpCommand = new HelpCommand()
  readonly start: StartCommand = new StartCommand()
  constructor(
    /* Your commands here */
  ) {
   }

}