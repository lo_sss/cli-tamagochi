import { IScreenControl } from './interfaces/screen-control';
import { ScreenControl } from './screen-control';
import { displayMenu } from './service';
import { GeneralManager } from './managers/general-manager';
import { Tamagochi } from "./tamagochi";
class MainGame {
  private tamagochi: Tamagochi;
  private startedLowering: boolean;
  private generalManager: any;
  private screenControl: IScreenControl;

  constructor() {
    this.tamagochi = new Tamagochi("Spirulin");
    this.generalManager = new GeneralManager(this.tamagochi)
    this.screenControl = new ScreenControl(this.generalManager, this.tamagochi)
    this.startedLowering = false;
  }

  startGame() {
    console.log("Starting Tamagochi Game");
    displayMenu();
    const stdin = process.openStdin();
    stdin.addListener("data", d => {
      if (!this.startedLowering) {
        this.generalManager.startTime()
        this.startedLowering = true;
      }
      this.readInput(d.toString().trim());
    });
  }

  readInput(input: string) {
    this.screenControl.readInput(input)
  }
}

export default MainGame;
