import { Screens } from './../enums/screen-type';
import { Igame } from '../interfaces/game';
import { GAME_TIME } from './../constants';
import { Tamagochi } from "../tamagochi";
import { ScreenControl } from '../screen-control';

export class GameManager implements Igame{
    private tamagochi: Tamagochi;
    constructor(tamagochi: Tamagochi) {
      this.tamagochi = tamagochi;
    }

    startTime(): void {
        setInterval(() => {
          this.lowerGame();
        }, GAME_TIME);
      }
    
      lowerGame(): void {
        this.tamagochi.game = this.tamagochi.game - 1
        if(this.tamagochi.game === 0){
          if (ScreenControl.CURRENT_SCREEN === Screens.MainScreen){
          console.log("I want to play...I want to play...")}
          setTimeout(() => {
            ScreenControl.updateScreen(this.tamagochi)
          }, 2000)
        } else{
          ScreenControl.updateScreen(this.tamagochi)
        }
      }
    
      gameMenu(): void {
        console.log(`Play with me... \n  Puzzle - 1  Boardgame -2  Dance - 3  Football - 4`)
      }
    
      gameMe(input: number): void {
        if(input !== 1 && input !== 2 && input !== 3 && input !== 4){
          console.log("You should choose between the options")
        }else{
        this.tamagochi.game = this.tamagochi.game + input;}
      }
}