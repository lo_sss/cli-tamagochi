import { Screens } from './../enums/screen-type';
import { Isleep } from '../interfaces/sleep';
import { SLEEP_TIME } from './../constants';
import { Tamagochi } from "../tamagochi";
import { ScreenControl } from '../screen-control';

export class SleepManager implements Isleep{
    private tamagochi: Tamagochi;
    constructor(tamagochi: Tamagochi) {
      this.tamagochi = tamagochi;
    }

    startTime(): void {
        setInterval(() => {
          this.lowerSleep();
        }, SLEEP_TIME);
      }
    
      lowerSleep(): void {
        this.tamagochi.sleep = this.tamagochi.sleep - 1
        if(this.tamagochi.sleep === 0){
          if (ScreenControl.CURRENT_SCREEN === Screens.MainScreen){
          console.log("I'm so sleepy. I just want to find my bed...")}
          setTimeout(() => {
            ScreenControl.updateScreen(this.tamagochi)
          }, 2000)
        } else {
          ScreenControl.updateScreen(this.tamagochi)
        }

      }
    
      sleepMenu(): void {
        console.log(`Can not sleep wihtout... \n  Teddy bear - 1  Goodnight wish - 2  Fairytale - 3  Goodnight song - 4`)
      }
    
      sleepMe(input: number): void {
        if(input !== 1 && input !== 2 && input !== 3 && input !== 4){
          console.log("You should choose between the options")
        }else {
        this.tamagochi.sleep = this.tamagochi.sleep + input;}
      }
}