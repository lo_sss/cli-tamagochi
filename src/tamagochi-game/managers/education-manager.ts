import { Screens } from './../enums/screen-type';
import { Ieducate } from '../interfaces/educate';
import { EDUCATION_TIME } from './../constants';
import { Tamagochi } from "../tamagochi";
import { ScreenControl } from '../screen-control';

export class EducationManager implements Ieducate{
    private tamagochi: Tamagochi;
    constructor(tamagochi: Tamagochi) {
      this.tamagochi = tamagochi;
    }

    startTime(): void {
        setInterval(() => {
          this.lowerEducation();
        }, EDUCATION_TIME);
      }
    
      lowerEducation(): void {
        this.tamagochi.education = this.tamagochi.education - 1
        if(this.tamagochi.education === 0){
          if (ScreenControl.CURRENT_SCREEN === Screens.MainScreen){
          console.log("Where is Paris? What is the sum of 8 and 15? Please teach me...")}
          setTimeout(() => {
            ScreenControl.updateScreen(this.tamagochi)
          }, 2000)
        } else {
          ScreenControl.updateScreen(this.tamagochi)
        }
      }
    
      educationMenu(): void {
        console.log(`Let's learn some... \n  History - 1  Geography -2  English - 3  Math - 4`)
      }
    
      educateMe(input: number): void {
        if(input !== 1 && input !== 2 && input !== 3 && input !== 4){
          console.log("You should choose between the options")
        }else{
        this.tamagochi.education = this.tamagochi.education + input;}
      }
}