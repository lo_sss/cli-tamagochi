import { ScreenControl } from './../screen-control';
import { Ifeed } from '../interfaces/feed';
import { FOOD_TIME } from "../constants";
import { Tamagochi } from "../tamagochi";
import { Screens } from '../enums/screen-type';
export class FoodManager implements Ifeed{
  private tamagochi: Tamagochi;
  constructor(tamagochi: Tamagochi) {
    this.tamagochi = tamagochi;
  }

  startTime(): void {
    setInterval(() => {
      this.lowerFood();
    }, FOOD_TIME);
  }

  lowerFood(): void {
    this.tamagochi.food = this.tamagochi.food - 1
    if(this.tamagochi.food === 0){
      if (ScreenControl.CURRENT_SCREEN === Screens.MainScreen){
      console.log("I'm so so hungry...")
    }
      setTimeout(() => {
        ScreenControl.updateScreen(this.tamagochi)
      }, 2000)
    }else {
      ScreenControl.updateScreen(this.tamagochi)
    }
  }

  feedMenu(): void {
    console.log(`Feed me with... \n Breakfast - 1  Salat -2  Desert - 3  Lamb - 4`);
  }

  feedMe(input: number): void {
    if(input !== 1 && input !== 2 && input !== 3 && input !== 4){
      console.log("You should choose between the options")
    } else {
    this.tamagochi.food = this.tamagochi.food + input;}
  }
}
