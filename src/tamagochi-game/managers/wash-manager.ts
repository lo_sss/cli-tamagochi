import { Screens } from './../enums/screen-type';
import { Iwash } from '../interfaces/wash';
import { WASH_TIME } from './../constants';
import { Tamagochi } from "../tamagochi";
import { ScreenControl } from '../screen-control';

export class WashManager implements Iwash{
    private tamagochi: Tamagochi;
    constructor(tamagochi: Tamagochi) {
      this.tamagochi = tamagochi;
    }
    startTime(): void {
        setInterval(() => {
          this.lowerWash();
        }, WASH_TIME);
      }
    
      lowerWash(): void {
        this.tamagochi.wash = this.tamagochi.wash - 1
        if(this.tamagochi.wash === 0){
          if (ScreenControl.CURRENT_SCREEN === Screens.MainScreen){
          console.log("I have black spot here and here and here...")}
          setTimeout(() => {
            ScreenControl.updateScreen(this.tamagochi)
          }, 2000)
        } else {
          ScreenControl.updateScreen(this.tamagochi)
        }
      }
    
      washMenu(): void {
        console.log(`It's time to... \n  Wash hands - 1  Wash teeths -2  Take a bath - 3  Take a shower - 4`)
      }
    
      washMe(input: number): void {
        if(input !== 1 && input !== 2 && input !== 3 && input !== 4){
          console.log("You shoud choose beteen the options")
        }else {
        this.tamagochi.wash = this.tamagochi.wash + input;}
      }
}