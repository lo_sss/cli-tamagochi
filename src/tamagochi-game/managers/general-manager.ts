import { Iwash } from './../interfaces/wash';
import { Ieducate } from './../interfaces/educate';
import { Igame } from './../interfaces/game';
import { Isleep } from './../interfaces/sleep';
import { Ifeed } from './../interfaces/feed';
import { IgenneralManager } from '../interfaces/general-manager';
import { Tamagochi } from './../tamagochi';
import { EducationManager } from './education-manager';
import { SleepManager } from './sleep-manager';
import { GameManager } from './game-manager';
import { FoodManager } from './food-manager';
import { WashManager } from './wash-manager';

export class GeneralManager implements IgenneralManager{
  private foodManager: Ifeed
  private sleepManager: Isleep
  private gameManager: Igame
  private educationManager: Ieducate
  private washManager: Iwash

  constructor(private readonly tamagochi: Tamagochi){
    this.foodManager = new FoodManager(tamagochi)
    this.sleepManager = new SleepManager(tamagochi)
    this.gameManager = new GameManager(tamagochi)
    this.educationManager = new EducationManager(tamagochi)
    this.washManager = new WashManager(tamagochi)
  }
  startTime(): void {
    this.foodManager.startTime()
    this.educationManager.startTime();
    this.gameManager.startTime();
    this.sleepManager.startTime();
    this.washManager.startTime();
  }

  feedMe(input: number): void {
    this.foodManager.feedMe(input)
  }

  educateMe(input: number): void {
    this.educationManager.educateMe(input)
  }

  gameMe(input: number): void {
    this.gameManager.gameMe(input)
  }

  washMe(input: number): void {
    this.washManager.washMe(input)
  }

  sleepMe(input: number): void {
    this.sleepManager.sleepMe(input)
  }

  displayFoodMenu(): void{
    this.foodManager.feedMenu()
  }

  displayGameMenu(): void{
    this.gameManager.gameMenu()
  }

  displayEducationMenu(): void{
    this.educationManager.educationMenu()
  }

  displaySleepMenu(): void{
    this.sleepManager.sleepMenu()
  }

  displayWashMenu(): void{
    this.washManager.washMenu()
  }
}
