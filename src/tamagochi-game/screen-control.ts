import { IScreenControl } from './interfaces/screen-control';
import { Itamagochi } from "./interfaces/tamagochi";
import { Screens } from "./enums/screen-type";
import { IgenneralManager } from "./interfaces/general-manager";
import { GeneralManager } from "./managers/general-manager";

export class ScreenControl implements IScreenControl{
  static CURRENT_SCREEN: Screens;
  private generalManager: IgenneralManager;
  private tamagochi: Itamagochi;
  constructor(generalManager: GeneralManager, tamagochi: Itamagochi) {
    this.generalManager = generalManager;
    this.tamagochi = tamagochi;
    ScreenControl.CURRENT_SCREEN = Screens.MainMenu;
  }

  public static updateScreen(tamagochi: Itamagochi) {
    if (ScreenControl.CURRENT_SCREEN === Screens.MainScreen) {
      tamagochi.print();
    }
  }

  readInput(input: string): void {
    switch (ScreenControl.CURRENT_SCREEN) {
      case Screens.MainMenu:
        this.mainMenuChoice(input);
        break;

      case Screens.Name:
        this.nameChoice(input);
        break;

      case Screens.MainScreen:
        this.mainScreenChoice(input.toLowerCase());
        break;

      case Screens.Food:
        this.foodChoice(input);
        break;

      case Screens.Education:
        this.educationChoice(input);
        break;

      case Screens.Game:
        this.gameChoice(input);
        break;

      case Screens.Wash:
        this.washChoice(input);
        break;

      case Screens.Sleep:
        this.sleepChoice(input);
        break;
    }
  }

  mainScreenChoice(input: string): void {
    if (
      input !== "f" &&
      input !== "e" &&
      input !== "g" &&
      input !== "w" &&
      input !== "s"
    ) {
      console.log("You should choose between options - F, E, G, W, S");
    }
    if (input === "f") {
      this.generalManager.displayFoodMenu();
      ScreenControl.CURRENT_SCREEN = Screens.Food;
    } else if (input === "e") {
      this.generalManager.displayEducationMenu();
      ScreenControl.CURRENT_SCREEN = Screens.Education;
    } else if (input === "g") {
      this.generalManager.displayGameMenu();
      ScreenControl.CURRENT_SCREEN = Screens.Game;
    } else if (input === "w") {
      this.generalManager.displayWashMenu();
      ScreenControl.CURRENT_SCREEN = Screens.Wash;
    } else if (input === "s") {
      this.generalManager.displaySleepMenu();
      ScreenControl.CURRENT_SCREEN = Screens.Sleep;
    }
  }

  mainMenuChoice(input: string): void {
    if (input === "2") {
      process.exit();
    }

    if (input === "1") {
      console.clear();
      console.log(
        "Please insert the name of your Tamagochi and Enter to take care of it"
      );
      ScreenControl.CURRENT_SCREEN = Screens.Name;
    }
  }

  printTamagochi(): void {
    console.clear();
    this.tamagochi.print();
    ScreenControl.CURRENT_SCREEN = Screens.MainScreen;
  }

  nameChoice(input: string): void {
    this.tamagochi.name = input;
    this.printTamagochi();
  }

  foodChoice(input: string): void {
    this.generalManager.feedMe(+input);
    this.printTamagochi();
  }

  educationChoice(input: string): void {
    this.generalManager.educateMe(+input);
    this.printTamagochi();
  }

  gameChoice(input: string): void {
    this.generalManager.gameMe(+input);
    this.printTamagochi();
  }

  washChoice(input: string): void {
    this.generalManager.washMe(+input);
    this.printTamagochi();
  }

  sleepChoice(input: string): void {
    this.generalManager.sleepMe(+input);
    this.printTamagochi();
  }
}
