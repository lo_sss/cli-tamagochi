export enum FoodType {
    Food,
    Breakfast,
    Salad,
    Dessert,
    Lamb
  }