export enum EducationType {
    Education,
    History,
    Geography,
    English,
    Math
  }