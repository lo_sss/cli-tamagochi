export enum GameType {
    Game, 
    Puzzle,
    Boardgame,
    Dance,
    Football
  }