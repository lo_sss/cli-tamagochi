import { Screens } from './enums/screen-type';
import { ScreenControl } from './screen-control';
import { Itamagochi } from './interfaces/tamagochi';
import { SleepType } from "./enums/sleep-type";
import { WashType } from "./enums/wash-type";
import { GameType } from "./enums/game-type";
import { EducationType } from "./enums/education-type";
import { FoodType } from "./enums/food-type";

export class Tamagochi implements Itamagochi{
  private _name: string;
  private _food: number;
  private _education: number;
  private _game: number;
  private _wash: number;
  private _sleep: number;

  constructor(name: string) {
    if (!name || name.length < 2 || name.length > 20) {
      throw new Error("Name must be between 2 and 20 characters!");
    }

    this._name = name;
    this._food = FoodType.Salad;
    this._education = EducationType.Geography;
    this._game = GameType.Boardgame;
    this._wash = WashType.Wash_teeths;
    this._sleep = SleepType.Goodnight_wish;
  }

  public get food(): number {
    return this._food;
  }

  public get education(): number {
    return this._education;
  }

  public get game(): number {
    return this._game;
  }

  public get wash(): number {
    return this._wash;
  }

  public get sleep(): number {
    return this._sleep;
  }

  public get name(): string {
    return this._name;
  }

  public set name(newName: string) {
    this._name = newName;
  }

  public set food(food: number) {
    this._food = this.validatedStat(food);
    this.checkIfDead();
  }

  public set education(education: number) {
    this._education = this.validatedStat(education);
    this.checkIfDead();
  }

  public set game(game: number) {
    this._game = this.validatedStat(game);
    this.checkIfDead();
  }

  public set wash(wash: number) {
    this._wash = this.validatedStat(wash);
    this.checkIfDead();
  }

  public set sleep(sleep: number) {
    this._sleep = this.validatedStat(sleep);
    this.checkIfDead();
  }

  validatedStat(newStat: number): number {
    if (newStat < 0) {
      return 0;
    }

    if (newStat > 10) {
      return 10;
    }

    return newStat;
  }

  checkIfDead(): void {
    if (
      this.food === 0 &&
      this.education === 0 &&
      this.game === 0 &&
      this.wash === 0 &&
      this.sleep === 0
    ) {
      ScreenControl.CURRENT_SCREEN = Screens.Dead
      this.printRIP();
      process.exit()
    }
  };

  public happyRate(): number {
    return this._food + this._education + this._game + this._wash + this._sleep;
  }

  public print(): void {
    console.clear()
    console.log(`${this._name} - Happy rate: ${this.happyRate()}/50
        

        \\\|/.._                            ${this.food}/10  F - food\r        
        / _  |||;._/ )                    ${this.education}/10  E - education\r
      _/@ @  ///  | (                     ${this.game}/10  G - game\r
     ( (__,      ,-\\ )\                    ${this.wash}/10  W - wash\r
      \'.\\_/ \_.\ '                          ${this.sleep}/10  S - sleep
        \`""\`\``);
  }

  printRIP(): void {
    console.clear();
    console.log(` 
                             .
                            -|-
                             |
                         .-\'~~~\`-.
                       .\'         \`.
                       |  R  I  P  |
                       |           |
                       |           |
                       \|           |
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^`);
  }
}
