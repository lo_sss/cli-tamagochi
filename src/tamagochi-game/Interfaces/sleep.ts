export interface Isleep {
  startTime(): void;

  lowerSleep(): void;

  sleepMenu(): void;

  sleepMe(input: number): void;
}
