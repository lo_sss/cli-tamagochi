export interface IScreenControl {
  
    readInput(input: string): void
  
    mainScreenChoice(input: string): void 
  
    mainMenuChoice(input: string): void 
  
    printTamagochi(): void
  
    nameChoice(input: string): void
  
    foodChoice(input: string): void
  
    educationChoice(input: string): void
  
    gameChoice(input: string): void
  
    washChoice(input: string): void
  
    sleepChoice(input: string): void
  }