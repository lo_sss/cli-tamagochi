export interface Iwash {
  startTime(): void;

  lowerWash(): void;

  washMenu(): void;

  washMe(input: number): void;
}
