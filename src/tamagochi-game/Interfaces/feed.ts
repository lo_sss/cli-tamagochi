export interface Ifeed {
  startTime(): void;

  lowerFood(): void;

  feedMenu(): void;

  feedMe(input: number): void;
}
