export interface Itamagochi {
  name: string
  validatedStat(newStat: number): number;

  checkIfDead(): void;

  happyRate(): number;

  print(): void;

  printRIP(): void;
}
