export interface Ieducate {
  startTime(): void;

  lowerEducation(): void;

  educationMenu(): void;

  educateMe(input: number): void;
}
