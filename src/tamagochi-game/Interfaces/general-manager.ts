export interface IgenneralManager {

  startTime(): void;

  feedMe(input: number): void;

  educateMe(input: number): void;

  gameMe(input: number): void;

  washMe(input: number): void;

  sleepMe(input: number): void;

  displayFoodMenu(): void;

  displayGameMenu(): void;

  displayEducationMenu(): void;

  displaySleepMenu(): void;

  displayWashMenu(): void;
}
