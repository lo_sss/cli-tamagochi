export interface Igame {
  startTime(): void;

  lowerGame(): void;

  gameMenu(): void;

  gameMe(input: number): void;
}
