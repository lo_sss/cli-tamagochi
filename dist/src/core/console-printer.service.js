"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ConsolePrinter {
    constructor() { }
    print(...texts) {
        console.log(...texts);
    }
}
exports.ConsolePrinter = ConsolePrinter;
