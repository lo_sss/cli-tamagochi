"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GameType;
(function (GameType) {
    GameType[GameType["Puzzle"] = 0] = "Puzzle";
    GameType[GameType["Boardgame"] = 1] = "Boardgame";
    GameType[GameType["Dance"] = 2] = "Dance";
    GameType[GameType["Football"] = 3] = "Football";
})(GameType = exports.GameType || (exports.GameType = {}));
