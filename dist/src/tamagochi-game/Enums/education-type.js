"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EducationType;
(function (EducationType) {
    EducationType[EducationType["History"] = 0] = "History";
    EducationType[EducationType["Geography"] = 1] = "Geography";
    EducationType[EducationType["English"] = 2] = "English";
    EducationType[EducationType["Math"] = 3] = "Math";
})(EducationType = exports.EducationType || (exports.EducationType = {}));
