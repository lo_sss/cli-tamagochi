"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var WashType;
(function (WashType) {
    WashType[WashType["Wash_hands"] = 0] = "Wash_hands";
    WashType[WashType["Wash_teeths"] = 1] = "Wash_teeths";
    WashType[WashType["Take_bath"] = 2] = "Take_bath";
    WashType[WashType["Take_shower"] = 3] = "Take_shower";
})(WashType = exports.WashType || (exports.WashType = {}));
