"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SleepType;
(function (SleepType) {
    SleepType[SleepType["Teddy_bear"] = 0] = "Teddy_bear";
    SleepType[SleepType["Goodnight_wish"] = 1] = "Goodnight_wish";
    SleepType[SleepType["Fairytale"] = 2] = "Fairytale";
    SleepType[SleepType["Goodnight_song"] = 3] = "Goodnight_song";
})(SleepType = exports.SleepType || (exports.SleepType = {}));
