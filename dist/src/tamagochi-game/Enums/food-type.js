"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var FoodType;
(function (FoodType) {
    FoodType[FoodType["Breakfast"] = 0] = "Breakfast";
    FoodType[FoodType["Salad"] = 1] = "Salad";
    FoodType[FoodType["Dessert"] = 2] = "Dessert";
    FoodType[FoodType["Lamb"] = 3] = "Lamb";
})(FoodType = exports.FoodType || (exports.FoodType = {}));
