"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tamagochi_1 = require("./tamagochi");
class MainGame {
    constructor() {
        this.tamagochi = new tamagochi_1.Tamagochi("Spirulin");
    }
    startGame() {
        console.log("Starting Tamagochi Game");
        console.log("please press a key to continue...");
        const stdin = process.openStdin();
        stdin.addListener("data", (d) => {
            const input = d.toString().trim();
            this.displayMenu();
            console.log();
            console.log("choice:");
            if (input === "3") {
                process.exit();
            }
            if (input === "1") {
                this.doGame();
            }
        });
    }
    displayMenu() {
        console.log("1. New Game");
        console.log("2. Load Game");
        console.log("3. Exit");
    }
    doGame() {
    }
}
exports.default = MainGame;
