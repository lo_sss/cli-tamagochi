"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sleep_type_1 = require("./Enums/sleep-type");
const wash_type_1 = require("./Enums/wash-type");
const game_type_1 = require("./Enums/game-type");
const education_type_1 = require("./Enums/education-type");
const food_type_1 = require("./Enums/food-type");
class Tamagochi {
    constructor(name) {
        if (!name || name.length < 2 || name.length > 20) {
            throw new Error("Name must be between 2 and 20 characters!");
        }
        this._name = name;
        this.food = food_type_1.FoodType.Salad;
        this.education = education_type_1.EducationType.Geography;
        this.game = game_type_1.GameType.Boardgame;
        this.wash = wash_type_1.WashType.Wash_teeths;
        this.sleep = sleep_type_1.SleepType.Goodnight_wish;
    }
}
exports.Tamagochi = Tamagochi;
