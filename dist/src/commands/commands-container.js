"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const start_1 = require("./start");
const help_1 = require("./help");
class CommandContainer {
    constructor(
    /* Your commands here */
    ) {
        // _command1: string
        this.help = new help_1.HelpCommand();
        this.start = new start_1.StartCommand();
    }
}
exports.CommandContainer = CommandContainer;
