
<img src="fish.jpg" alt="drawing" width="200"/>

## OOP Project - CLI tool

### Tamagochi Game

#### This is a tamagochi game created as part of Telerik Academy OOP Project.

#### Description

You create your tamagochi which has different stats. You need to wash, feed him etc. to keep him healthy and happy. His stats lower over time and certain foods and other properties satisfy him more than others.

#### Abilities

- Food
- Education
- Game
- Wash
- Sleep

#### Technologies

- Typescript
- OOP

#### Installation

- Clone the repo
- Run npm install
- In main dir run "ts-node main.ts start"
- Enjoy

